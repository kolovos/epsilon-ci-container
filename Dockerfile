FROM eclipse-temurin:17

### STAGE 1 ###
# Install Packages
###############

# Disable interactivity for install of git
ARG DEBIAN_FRONTEND=noninteractive

RUN echo "Running on arch '${TARGETARCH}'"

RUN apt-get update

RUN apt-get upgrade --yes

RUN apt-get install --yes --no-install-recommends \
xvfb \
dbus-x11 \
firefox \
libxtst6 \
libxrender1 \
file \
wget \
git \
nano

RUN apt-get clean

RUN  rm -rf /var/lib/apt/lists/*

### STAGE 2 ###
# Install eclipse director
###############

WORKDIR /

# Specifications for download of the director application

ARG ECLIPSE_OPERATING_SYSTEM="linux64"
ARG ECLIPSE_INSTALLER_ARCHIVE_FILE="eclipse-inst-${ECLIPSE_OPERATING_SYSTEM}.tar.gz"
ARG ECLIPSE_INSTALLER_DOWNLOAD_URL="https://www.eclipse.org/downloads/download.php?file=/oomph/products/${ECLIPSE_INSTALLER_ARCHIVE_FILE}&r=1"

RUN echo "Downloading from '${ECLIPSE_INSTALLER_DOWNLOAD_URL}'"
RUN wget --quiet --max-redirect=1 --output-document="${ECLIPSE_INSTALLER_ARCHIVE_FILE}" "${ECLIPSE_INSTALLER_DOWNLOAD_URL}"
RUN tar --extract --warning=no-unknown-keyword --file=${ECLIPSE_INSTALLER_ARCHIVE_FILE}
RUN rm ${ECLIPSE_INSTALLER_ARCHIVE_FILE}

### STAGE 3 ###
# Install "base" eclipse
###############

# Run the following commands in the eclipse installation directory
WORKDIR /eclipse-installer

# Run eclipse P2 director to install an Epsilon setup for executing ant
# https://wiki.eclipse.org/Equinox/p2/Director_application
# https://help.eclipse.org/latest/index.jsp?topic=/org.eclipse.platform.doc.isv/guide/p2_director.html

# Command-line options for the eclipse platform binary:
# https://help.eclipse.org/latest/topic/org.eclipse.platform.doc.user/tasks/running_eclipse.htm

# Specifications for the installation to be performed
# Repositories that the P2 director should use as sources to obtain the Installable Units (IUs)

ARG P2_REPOSITORIES=\
https://mirror.aarnet.edu.au/pub/eclipse/releases/latest,\
https://mirror.aarnet.edu.au/pub/eclipse/eclipse/updates/latest,\
https://mirror.aarnet.edu.au/pub/eclipse/epsilon/interim,\
https://mirror.aarnet.edu.au/pub/eclipse/emfatic/update,\
https://mirror.aarnet.edu.au/pub/eclipse/modeling/gmp/gmf-tooling/updates/releases,\
https://mirror.aarnet.edu.au/pub/eclipse/birt/update-site/latest,\
https://committed-consulting.gitlab.io/mde-devops/eclipse-ant-contributions/


## Eclipse SDK
# org.eclipse.sdk.ide,\
# # Epsilon
# org.eclipse.epsilon.core.dt.feature.feature.group,\
# org.eclipse.epsilon.core.feature.feature.group,\
# org.eclipse.epsilon.emf.dt.feature.feature.group,\
# org.eclipse.epsilon.emf.feature.feature.group,\
# org.eclipse.epsilon.eunit.dt.emf.feature.feature.group,\
# org.eclipse.epsilon.evl.emf.validation.feature.feature.group,\
# # Emfatic
# org.eclipse.emf,\
# org.eclipse.emf.cdo.sdk.feature.group,\
# org.eclipse.emf.emfatic.core,\
# # Some Eclipse Resource factories
# org.eclipse.wst.wsdl,\
# org.eclipse.xsd.sdk.feature.group \


ARG INSTALLABLE_UNITS=\
org.eclipse.sdk.ide,\
org.eclipse.epsilon.core.dt.feature.feature.group,\
org.eclipse.epsilon.core.feature.feature.group,\
org.eclipse.epsilon.emf.dt.feature.feature.group,\
org.eclipse.epsilon.emf.feature.feature.group,\
org.eclipse.epsilon.eunit.dt.emf.feature.feature.group,\
org.eclipse.epsilon.evl.emf.validation.feature.feature.group,\
org.eclipse.epsilon.hutn.dt.feature.feature.group,\
org.eclipse.emf,\
org.eclipse.emf.cdo.sdk.feature.group,\
org.eclipse.emf.emfatic.core,\
org.eclipse.wst.wsdl,\
org.eclipse.xsd.sdk.feature.group,\
net.jgsuess.util.ant.feature.group


# # Start the install
# RUN ./eclipse-inst \
# # Do not show the splash screen
# -nosplash \
# # Run terminal log
# -consoleLog \
# # Select the director application
# -application org.eclipse.equinox.p2.director \
# # Use these repositories as source of installable units, and follow their upstream update site locations (if supplied)
# -repository ${P2_REPOSITORIES} \
# # Install these installable units
# -installIU ${INSTALLABLE_UNITS} \
# # Use this profile to install
# -profile SDKProfile \
# # Turn update on for this profile
# -profileProperties org.eclipse.update.install.features=true \
# # Install into this directory
# -destination /epsilon

RUN ./eclipse-inst \
-nosplash \
-consoleLog \
-application org.eclipse.equinox.p2.director \
-repository ${P2_REPOSITORIES} \
-installIU ${INSTALLABLE_UNITS} \
-profile SDKProfile \
-profileProperties org.eclipse.update.install.features=true \
-destination /epsilon

### STAGE 5 ###
# Final steps to complete the container build
###############

WORKDIR /workspace

COPY /docker-entrypoint.sh /
RUN chmod 755 /docker-entrypoint.sh

# set the entrypoint
ENTRYPOINT [ "/docker-entrypoint.sh" ]

# advertise volume mount
VOLUME [ "/workspace" ]
